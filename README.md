![Logo Deface](https://deface.app/images/deface-banner.jpg "Logo Deface")

# deface

### Deface is an app that prevents Facebook from reading people’s messages.

The recent democratization of encrypted messaging (e.g [Signal](https://signal.org/), [Wire](https://wire.com/en/)) has helped protecting the communications of millions of people. Conversely, other areas of our digital lives have seen little progress when it comes to privacy. Social media feeds is one them. Given their open nature, how can we bring some degree of privacy to public conversations?

Deface piggybacks on Facebook and makes it hard for the platform to read people's content. It captures messages posted in public feeds, encrypts them, and enables users to access them. Meanwhile, it leaves Facebook and third party organizations with data that's costly to access.

Deface's general documentation and demo video are located [on the project's homepage](https://deface.app). It comprises of design considerations and technical principles. 

---

This repository is an entry point to Deface's several repositories. Getting familiar with them and understanding how they interact will help you grasp how Deface works as a whole.

* [*deface-chrome*](https://gitlab.com/deface/deface-chrome): Google Chrome extension for Deface. It injects Facebook pages with a script that captures, encrypts and decrypts user posts.

* [*deface-server*](https://gitlab.com/deface/deface-server): Currently archived, this repository will contain server logic for handling Deface's public-key encrypted commuications.

* [*deface-www*](https://gitlab.com/deface/deface-www): Home page for the project's documentation. 

## 🚨🚨🚨 Disclaimer 🚨🚨🚨

Deface currently exists in its alpha 4 version. It is not suitable for use by anyone. Please be patient and courteous: don't use Deface in the wild before it reaches a stable state.

Additionally, Deface was recently refactored to accommodate new strategies in favor of user privacy. The code is currently messy and undocumented. Beware of the dragons 🐉🐉🐉

## Tracks of work

~~For a granular view of current efforts, please refer to this project's [issue board](https://gitlab.com/groups/deface/-/boards). Below is a summary of tracks of work for 2019.~~

Refer to the [project's homepage](https://deface.app/mission#challenges).

__1. 🛡 Security and privacy:__
  - Review design with infosec experts
  - Explore the cost of our proof-of-work mechanism
  - Seek security audit

__2. ☔️ Content script robustness:__
  - Make content scripts resilient to Facebook's countermeasures
  
__3. 📱 Cross-platform experience:__
  - Adapt deface-chrome for Firefox
  - Develop Deface for iOS and Android

__4. 🔐 Public-key encryption features:__
  - Develop Deface's implementation of the [Signal protocal](https://signal.org/docs/)
  - Develop deface-server
  - Enable cross-platform login

__5. ⛑ User experience:__
  - Provide in-app security information
  - Develop features for data recovery

__6. 😎 Accessibility:__
  - Investigate Deface's impact on accessibility


## Contribute

We need help, and welcome all contributions! If you're not sure where to start, reading through [the project's homepage](https://deface.app) will be helpful. If you want to contribute with code, ~~please have a look at our [issue board](https://gitlab.com/groups/deface/-/boards)~~. Feel free to submit issues and (even better) merge requests. 

This project defaults to [standard](https://standardjs.com/) code style. It is a clean codestyle, and its adoption is increasing significantly, making the code that we write familiar to the majority of the developers.

## Code of Conduct

This project follows a [Code of Conduct](https://gitlab.com/deface/deface/blob/master/CODE-OF-CONDUCT.md) based on [NPM's](https://www.npmjs.com/policies/conduct).

## License

MIT
